var	express = require( 'express' ),
    morgan = require('morgan'),
    cookieParser = require('cookie-parser'),
    cookieSession = require('cookie-session'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    errorHandler = require('errorhandler'),
    index = require( './server/routes/index.js');


var app = express();
var iniparser = require( 'iniparser' );
var config = iniparser.parseSync( './config.ini' );

// Set the view engine
app.set( 'view engine', 'ejs' );
// Where to find the view files
app.set ('views', './server/views' );
// Mark the public dir as a static dir
app.use( express.static( './public' ) );

//Middleware
app.use( errorHandler() );

// A route to the homepage = will render a view
app.use('/', index);


app.listen(8080);
console.log('Server listening on http://localhost:8080');
